﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public float playerSpeed = 0f;

    private AudioSource playerSounds;
    private SpriteRenderer playerSprite;
    private EdgeCollider2D playerCollider;

	// Use this for initialization
	void Start ()
    {
        var player = GameObject.FindGameObjectWithTag("Player");

        playerSounds = player.GetComponent<AudioSource>();
        playerSprite = player.GetComponent<SpriteRenderer>();
        playerCollider = player.GetComponent<EdgeCollider2D>();
       
        if((playerSounds == null)|| (playerSprite == null) || (playerCollider == null) || (playerSpeed == 0f))
        {
            Debug.Log("The player needs speed/audio/collider/sprite renderer :)");
        }  
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.position += Vector3.up * playerSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.position += Vector3.down * playerSpeed * Time.deltaTime;
        }
    }
}
